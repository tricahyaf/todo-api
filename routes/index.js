const express = require("express")
const route = express.Router()

const { findAllTodos, getTodoById, createNewTodo, updateTodo, destroyTodo} = require ('../controller/todosController.js')

route.get('/todo', findAllTodos);
route.get('/todo/:id', getTodoById);
route.post('/todo', createNewTodo);
route.patch('/todo/:id', updateTodo);
route.delete('/todo/:id', destroyTodo);

module.exports = route;