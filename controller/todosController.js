const {Todo} = require("../models");

const findAllTodos = async (req, res) => {
    try {
        const data = await Todo.findAll()

        const result = {
            status: 'ok',
            data: data
        }

        res.json(result)
    } catch (error) {
        console.log(error, '<<< error find all todo')
    }
}

const getTodoById = async (req, res) => {
    try {
        const { id } = req.params

        const data = await Todo.findByPk(id)
        if (data === null) {
            return res.status(404).json({
                status: 'failed',
                message: `data todo with id ${id} is not found`,
            })
        }

        res.json({
            status: 'ok',
            message: data
        })
    } catch (error) {
        console.log(error, '<<< error get todo by id')
    }
}

const createNewTodo = async (req, res) => {
    try {
        const {title} = req.body

        const newTodo = await Todo.create({title: title})

        res.status(201).json({
            status: 'ok',
            data: {
                id: newTodo.id,
                title: newTodo.todo,
                createdAt: newTodo.createdAt,
                updatedAt: newTodo.updatedAt
            }
        })
    } catch (error) {
        console.log(error, 'error create new Todo')
    }
}

const updateTodo = async (req, res) => {
    try {
        const {id} = req.params
        const { title } = req.body
        const todo = await Todo.findByPk(id)

        if (!todo) {
            return res.status(404).json({
                status: 'failed',
                message: `data todo with id ${id} is not exists`
            })
        }

        todo.title = title
        todo.updatedAt = new Date()

        todo.save()

        res.json({
            status:'ok',
            data: {
                id: todo.id,
                title: todo.title,
                createdAt: todo.createdAt,
                updatedAt: todo.updatedAt
            }
        })
    } catch (error) {
        console.log(error, '<-- error update todo')
    }
}

const destroyTodo = async (req, res) => {
    try {
        const { id } = req.params

        const todo = await Todo.findByPk(id)

        if (!todo) {
            return res.status(404).json({
                status: 'failed',
                message: `data todo with id ${id} is not exist`
            })
        }

        todo.destroy()

        res.json({
            status: 'ok',
            message: `success delete todo with id ${id}`
        })
    } catch (error) {
        console.log(error, '<-- error destroy todo')
    }
}

module.exports = { findAllTodos, getTodoById, createNewTodo, updateTodo, destroyTodo };