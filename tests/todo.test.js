const app = require('../index')
const request = require('supertest');
const { createNewTodo, updateTodo } = require('../data/todo.test.data.js');

describe("POST /api/todo", () => {
    test("should create a product", async () => {
        return request(app)
            .post("/api/todo")
            .send(createNewTodo)
            .expect(201)
            .then(({ body })=>{
                todoId = body.data.todoId
            })

    });
});

describe("GET /todo", () => {
    it("should return all title", async() => {
        return request(app)
            .get('/api/todo')
            .expect('Content-Type', /json/)
            .expect(200)
    });
});

describe("GET /todo/:id", () => {
    it('should return all title', async () => {
        return request(app)
            .get('/api/todo/:id')
            .expect(200)
            .expect('Content-Type', /json/)
    });
})

describe("POST /todo", () => {
    it("should create title", async () => {
        return request(app)
            .post('/api/todo')
            .send(createNewTodo)
            .expect(201)
    });
});

describe("PATCH /todo:id", () => {
    it("should update title", async () => {
        return request(app)
            .patch('/api/todo/:id')
            .send(updateTodo)
            .expect(201)
    });
});

describe("DELETE /todo/:id", () => {
    it("should delete title", async () => {
        return request(app)
            .destroy('/api/todo/:id')
            .expect(410)
    });
});