const express = require('express')
const app = express()
const port = 3030
const router = require('./routes/index');

app.use(express.json());
app.use(express.urlencoded({extended:true}));

app.use('/api', router)

app.listen(port, () => console.log(`server running on port ${port}`))